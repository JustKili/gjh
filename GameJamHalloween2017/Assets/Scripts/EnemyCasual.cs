﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyCasual : MonoBehaviour {

	float lifePoints;
	float maxLifePoints = 10;
	float damage = 1;
	 int direction = 1;
	 float enemySpeed = 1f;
	public Image healthBar;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		lifePoints = maxLifePoints;
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	void FixedUpdate () {
		rb.velocity = Vector2.right * direction * enemySpeed;
	}

	public void TakeDamage(float amt){
		lifePoints -= amt;
		UpdateHealthBar ();
		if (lifePoints <= 0) {
			Die ();
		}
	}

	void Die(){
		//give player points
		//disappear or lay down and disable collider
		Destroy(this.gameObject);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player")
			coll.gameObject.GetComponent<Player> ().TakeDamage (damage);
	}

	void UpdateHealthBar()
	{
		healthBar.fillAmount = lifePoints / maxLifePoints;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "DirectionChanger") {
			direction = direction * -1;
			Debug.Log ("Enemy Collided With DirectionChanger");
		}
	}
}
