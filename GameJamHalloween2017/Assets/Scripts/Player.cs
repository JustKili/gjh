﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	float curHp;
	float maxHp = 10;
	public Image healthBar;


	// Use this for initialization
	void Start () {
		curHp = maxHp;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TakeDamage(float amt){
		curHp -= amt;
		GetComponent<PlayerMovement> ().PushBack ();
		UpdateHealthBar ();
		if (curHp <= 0) {
			Die ();
		}
	}

	public bool Heal(float amt)
	{
		if (curHp >= maxHp) {
			return false;
		}
		curHp += amt;
		if(curHp > maxHp )
		{
			curHp = maxHp;
		}

		UpdateHealthBar ();
		return true;
	}

	void Die(){
		Debug.Log ("GAME OVER");
		Time.timeScale = 0;
	}

	void UpdateHealthBar()
	{
		healthBar.fillAmount = curHp / maxHp;
	}
}
