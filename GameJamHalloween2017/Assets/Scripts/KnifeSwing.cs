﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeSwing : MonoBehaviour {

	private Collider2D myColl;
	private float knifeDmg = 2;

	// Use this for initialization
	void Start () {
		myColl = GetComponent<Collider2D> ();
		myColl.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			EnableColl ();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("WEGOTACOLLISION OVER HERE");
		if(other.tag =="Enemy")
		{
			Debug.Log ("WE FKIN HIT AN ENEMY");
			other.GetComponent<EnemyCasual> ().TakeDamage(knifeDmg);
		}
	}

	public void EnableColl(){
		myColl.enabled = true;
	}

	public void DisableColl(){
		myColl.enabled = false;
	}
}
