﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour {

	public bool playerOnlyTrigger;
	public GameObject objectToTrigger;
	Triggerable trig;
	// Use this for initialization
	void Start () {
	}

	void Awake()
	{
		trig = (Triggerable)objectToTrigger.GetComponent < Triggerable> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (playerOnlyTrigger && coll.gameObject.tag == "Player") {
			trig.TriggerEnterObject ();
			Debug.Log ("PlayerTriggeredObject");
		} else {
			Debug.Log ("Smth else triggeredObject");
		}

	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if (playerOnlyTrigger && coll.gameObject.tag == "Player") {
			trig.TriggerExitObject ();
			Debug.Log ("PlayerLeftTrigger");
		} else {
			Debug.Log ("Smth else left Trigger");
		}
	}
}
