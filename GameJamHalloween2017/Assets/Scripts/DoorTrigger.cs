﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : Triggerable {

	public SpriteRenderer sr;
	public Sprite doorClosedSprite;
	public Sprite doorOpenSprite;
	public Collider2D doorCollider;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public override void TriggerEnterObject()
	{
		sr.sprite = doorOpenSprite;
		doorCollider.enabled = false;
	}

	public override void TriggerExitObject()
	{
		//ADD ANIMATION
		sr.sprite = doorClosedSprite;
		doorCollider.enabled = true;
	}
}
