﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	float moveSpeed = 4f;
	float jumpHeight = 5f;
	bool playerMovIsDisabled = false;
	Rigidbody2D rb2d;
	Collider2D c2d;
	public KnifeSwing knifeScript;
	public Transform groundCheck;
	public LayerMask groundMask;
	public float groundCheckRad;
	bool isGrounded = false;

	bool AHold;
	bool DHold;
	bool SpacePressed;

	[Header("KNIFE STUFF")]
	public Animator knifeAnim;


	// Use this for initialization
	void Start () {
		c2d = GetComponent<Collider2D> ();
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!playerMovIsDisabled) {
			if (Input.GetKeyDown (KeyCode.D)) {
				DHold = true;
			} else if (Input.GetKeyUp (KeyCode.D)) {
				DHold = false;
			}
			if (Input.GetKeyDown (KeyCode.A)) {
				AHold = true;
			} else if (Input.GetKeyUp (KeyCode.A)) {
				AHold = false;
			}
			if (Input.GetKeyDown (KeyCode.Space)) {
				SpacePressed = true;
				Debug.Log ("Space PRESSED");
			}

			/*
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			knifeScript.EnableColl ();
			knifeAnim.Play ("KnifeSwingAnimation");
		}
		*/
		}
	}

	void FixedUpdate(){
		isGrounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRad, groundMask);
		if (DHold) {
			//rb2d.AddForce(new Vector2 (moveSpeed*3, 0));
			rb2d.velocity = new Vector2 (moveSpeed, rb2d.velocity.y);
		}
		if (AHold) {
			//rb2d.AddForce(new Vector2 (-moveSpeed*3, 0));
			rb2d.velocity = new Vector2 (-moveSpeed, rb2d.velocity.y);
		}
//		else {
//			rb2d.velocity = new Vector2 (0, rb2d.velocity.y);
//		}
		if (SpacePressed && isGrounded && rb2d.velocity.y == 0f) {
			rb2d.velocity = new Vector2 (rb2d.velocity.x, jumpHeight);
			SpacePressed = false;
			Debug.Log ("JUMP EXECUTED");
		}
		else{
			SpacePressed = false;
		}

		//Snappy controls BRUH
		if (!AHold && !DHold) {
			rb2d.velocity = new Vector2 (0f, rb2d.velocity.y);
		}
	}

	public void PushBack(){
		toggleMovementAllowed ();
		rb2d.AddForce(new Vector2(-1400, 0f));	//????????????
		Debug.Log("Pushed back");
		toggleMovementAllowed ();
	}

	public void DisableKnifeColl(){
		knifeScript.DisableColl ();
	}

	public void toggleMovementAllowed()
	{
		playerMovIsDisabled = !playerMovIsDisabled;
	}
}
