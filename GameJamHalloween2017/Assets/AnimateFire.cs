﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateFire : MonoBehaviour {

	public SpriteRenderer sr;
	public Sprite[] sprites;
	int currentSprite = 0;
	float spriteDuration = .1f;
	float currentSpriteDuration = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (currentSpriteDuration < spriteDuration) {
			currentSpriteDuration += Time.deltaTime;
		} 
		else {
			//if (currentSprite >= sprites.Length) {
			//	currentSprite = 0;
				sr.sprite = sprites [currentSprite];
				currentSprite++;
				
			if (currentSprite > sprites.Length -1) {
				currentSprite = 0;
			}
			currentSpriteDuration = 0;
		}

	}
}
