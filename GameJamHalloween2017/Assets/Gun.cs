﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour {

	Camera playerCam;
	public Transform muzzle;
	public GameObject bullet;
	public float shootCD;
	float currentCD = 0;
	public float damage;

	// Use this for initialization
	void Start () {
		playerCam = Camera.main;
		if (muzzle == null) {
			Debug.Log ("No muzzle on Gun");
		}
		if (bullet == null) {
			Debug.Log ("No bullet on Gun");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (currentCD > 0) {
			currentCD -= Time.deltaTime;
		}
		if (Input.GetButtonDown("Fire1")&& currentCD <= 0) {
			Shoot ();
			currentCD = shootCD;
		}

	}

	void Shoot(){
		GameObject tempBullet = Instantiate (bullet, muzzle.position, this.transform.rotation) as GameObject;
		tempBullet.GetComponent<Bullet> ().TakeStats (damage);
		tempBullet.GetComponent<Bullet> ().ShootMe ();
	}
}
