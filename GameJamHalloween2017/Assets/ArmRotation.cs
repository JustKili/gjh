﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {

	int rotOffset = 0;
	Camera playerCam;

	// Use this for initialization
	void Start () {
		playerCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 difference = playerCam.ScreenToWorldPoint (Input.mousePosition) - transform.position;
		difference.Normalize ();

		float rotZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler (0f, 0f, rotZ + rotOffset);
	}
}
