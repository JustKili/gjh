﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour {

	public float healAmount = 5f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
		
	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("BOTTLE COLLIDED WITH PLAYER" );
		if (other.gameObject.tag == "Player") {

			if (other.gameObject.GetComponent<Player> ().Heal (healAmount)) {
				Destroy (this.gameObject);
			} 
		}
	}
}
