﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	float damage;
	public float moveSpeed = 50;
	Rigidbody2D rb2d;

	Camera playerCam;

	// Use this for initialization
	void Awake () {
		playerCam = Camera.main;
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Enemy") 
			coll.gameObject.GetComponent<EnemyCasual> ().TakeDamage (damage);	//change to pick up enemy script type~
		if(!(coll.gameObject.tag == "DirectionChanger"))
			Die ();
		
		
	}

	public void TakeStats(float dmg){
		damage = dmg;
	}

	public void ShootMe(){
		//rb2d.AddRelativeForce(new Vector3 (0,moveSpeed));
		rb2d.AddRelativeForce(transform.rotation*Vector2.right*moveSpeed);
	}

	void Die(){
		Destroy (this.gameObject);
	}
}
